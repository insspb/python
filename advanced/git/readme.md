# Git

Гит является основной системой контроля кода в большинстве opensource и частных проектов. Об его использовании написаны книги. В данной заметке я отмечу те нестандартные моменты, которые часто нужны лично мне.

## Добавление стороннего репозитория и кода из него

Все действия происходят в уже склонированном репозитории. Мы добавляем либо репозиторий апстрима, либо в целом можем добавить другой репозиторий. Например репозиторий из gitlab к github.

Давайте повторим и сначала склонируем какой-либо проект с гитлаба. В примере репозиторий под один из заказов с фриланса.

```bash
git@gitlab.com:insspb/exporter.git
cd exporter/
```

На этом этапе мы видим код и ветки, которые сделали мы сами. Но заказчик выложил его часть кода(фронтенд) на гитхаб и просит нас вести код там же. Что же делать?!

Давайте соединим два проекта в один.

Сначала добавим удалённый репозиторий и получим информацию о том, что там есть.

```bash
git remote add upstream git@github.com:mintsev/zendesk_exporter.git
git fetch upstream
```

Теперь создадим отдельную ветку в своём репозитории и соединим её с тем, что есть в удалённом.

```bash
git checkout -b origin/html
git merge upstream/master --allow-unrelated-histories
git push --set-upstream origin origin/html
```

Так как истории проектов начались в разное время и не связанные между собой, мы используем флаг `--allow-unrelated-histories`, чтобы связать всё вместе. Такой пул создаст `merge commit` с описанием произошедшего в другой ветке. При таком подходе сохраняется вся история коммитов и их авторов.

В некоторых случаях такая запись в историю не нужна, а хочется замаскировать всю историю сторонней ветки в один коммит. Сделать это можно следующим образом. Теже команды, с изменениями.

```bash
git checkout -b html
git merge upstream/master --allow-unrelated-histories --squash
git commit -m 'One message for remote'
git push --set-upstream origin html

```

В чём разница подходов? Вот лог изменений для первого варианта. В нём вы можете увидеть коммит от `eugborisov` и `Anton`, которые были сделаны на github.

```text
commit 966ec0f4bbffa46d56a8a7a1a951711154281d3b (HEAD -> origin/html, origin/origin/html)
Merge: 7ea19a7 36acfcb
Author: Andrey Shpak <ashpak@ashpak.ru>
Date:   Tue Jul 2 11:49:34 2019 +0300

    Merge remote-tracking branch 'upstream/master' into origin/html

commit 36acfcb3a2cfc25c105df1b15e29a8d474ba581d (upstream/master)
Merge: 311c332 0327fdf
Author: eugborisov <eug.borisov91@gmail.com>
Date:   Tue Jul 2 00:37:47 2019 +0400

    Merge pull request #1 from eugborisov/first_front-end_branch

    Initial commit

commit 0327fdf0803b07565a998643ae5bf4baac3f21b6
Author: eugborisov <eug.borisov91@gmail.com>
Date:   Tue Jul 2 00:33:18 2019 +0400

    Initial commit

commit 7ea19a7f7b99c04d158dae0a9d408bfe9a572f4d (origin/master, origin/HEAD, master)
Author: Andrey Shpak <ashpak@ashpak.ru>
Date:   Sun Apr 21 08:08:45 2019 +0300

    Project init

commit 311c3320ad9e4a88c0dbeff2ccbaa24527dfca64
Author: Anton <mintsev@gmail.com>
Date:   Thu Apr 18 17:06:28 2019 +0300

    Initial commit

```

Во втором случае история будет совсем другой. И коммит от `eugborisov` будет скрыт. Это не совсем честно по отношению к авторству, но во многих ситуациях может позволить вести более понятную историю коммитов.

```text
insspb@insspb-notebook:~/git_test/exporter$ git log
commit 2e38612f2da6700f6e683d043f098c13730f0b7e (HEAD -> html, origin/html)
Author: Andrey Shpak <ashpak@ashpak.ru>
Date:   Tue Jul 2 12:16:11 2019 +0300

    One message for remote

commit 7ea19a7f7b99c04d158dae0a9d408bfe9a572f4d (origin/master, origin/HEAD, master)
Author: Andrey Shpak <ashpak@ashpak.ru>
Date:   Sun Apr 21 08:08:45 2019 +0300

    Project init
```

## Проверка кода из пул реквестов

Теперь перейдём к несколько другой теме.

* В open source проектах не редко бывает так, что pull request остаётся без внимания очень долгое время. После чего пройденные этапы CI/CL становятся устаревшими, в связи с тем, что добавилась масса другого кода в проект.
* Либо как альтернатива вы хотите подправить код из какого-то PR собственными силами и внести изменения не дожидаясь оригинального автора.
* Ну и последний случай, когда вы хотите сделать один коммит для нескольких чужих PR, чтобы облегчить процесс рассмотрения maintenance team.

Начнём с того, что клонируем наш форк, 1в1 соответствующий оригиналу.

```bash
git@github.com:insspb/cookiecutter.git
```

Теперь нужно добавить ссылку на головной проект, скачать данные с него и создать новый бранч, в котором мы будем работать.

```bash
git remote add upstream git@github.com:cookiecutter/cookiecutter.git
git fetch upstream
git checkout -b version_remove
```

Теперь у нас есть три команды на выбор, которые делают практически одно и тоже, с маленькими особенностями.

### Вариант 1

* Сделать пулл реквест в свой бранч без дополнительных комманд.
  При его создании будет создан merge commit в котором будет ссылка на то, откуда и куда.
* Авторство оригинального коммита будет утеряно.
* Вся последующая работа будет в отдельных коммитах.

```bash
git pull upstream refs/pull/1178/head
```

### Вариант 2

* Сделать пулл реквест в свой бранч с `--rebase`.
  При его создании не будет создан какой либо коммит.
* Авторство оригинального коммита останется.
* История правок будет не логичной с точки зрения времени.
  В истории оригинального проекта появятся коммиты, которые были сделаны автором PR в то время, когда они были сделаны в PR. Это может несколько запутать.
* Вся последующая работа будет в отдельных коммитах.

```bash
git pull upstream refs/pull/1178/head --rebase
```

### Вариант 3

* Сделать пулл реквест в свой бранч с `--squash`.
  При его создании не будет создан какой либо коммит.
* Авторство оригинального коммита будет утеряно.
* Вы можете внести измения в код PR до того как сделать коммит с новым именем.

```bash
git pull upstream refs/pull/1178/head --squash
[do some work]
git commit -m 'Some message'
```

# Маскировка файла как "не изменился".

Иногда требуется, чтобы какой-то файл был в репозитории. Например файл с конфигурацией подключения к БД.  
Однако в локальной установке у того же файла будут совсем другие значения.  
Чтобы гит не предлагал всё время добавить этот файл в репозиторий требуется сделать следующее:

* Добавить файл в `.gitignore`
* Ввести команду `git update-index --assume-unchanged <file>`
