# Управление схемой базы данных с Alembic

Во многих проектах мне приходится работать с базами данных. Даже для проектов, где требуется просто собрать какие-то данные с интернета и выдать в итоге cvs работать с базой данных зачастую выгоднее. Это позволяет вести служебные таблицы, проще отслеживать дубликаты, проще отслеживать собранные и не собранные данные и так далее и тому подобное.

Проблема всегда заключается в том, что очень сложно изначально разработать идеальную структуру базы данных. В процессе разработки программного продукта всегда или почти всегда возникает необходимость добавить или удалить ту или иную таблицу, добавить или изменить колонку в таблице, индексы, связи. Делать это вручную сложно, отнимает кучу времени, а если проект уже запущен у клиента, то придётся проводить аналогичные действия у клиента. и не дай бог забыть тот или иной шаг. Ужас одним словом.

Для решения этой проблемы существует понятие миграций базы данных.

Миграции можно писать самостоятельно, вручную, а можно отдать этот участок работы специализированному софту. Для ``SQLAlchemy`` таким софтом является ``Alembic``.

В этой заметке проверенный алгоритм работы с ним на примере пустого приложения.

Всё тоже самое есть в официальной инструкции, там даже больше. А тут заметки лично для себя, выдержки.

## Установка

```bash
pip install alembic
# Из корня проекта:
alembic init [any_name]
```

В файле `alembic.ini` настроить переменную `sqlalchemy.url`, она должна соответствовать подключению к БД.

К сожалению невозможно избежать изменения этой переменной в `alembic.ini`, потому что она используется не только в `env.py`, но и в других модулях проекта.

### Использование функции автогенерации

Самой главной изюменкой ``Alembic`` является присутствие в ней функции автогенерации миграций. Для этого придётся чуть-чуть помучаться с настройками поболее.

Для использования этой функции в файле `env.py` нужно настроить указание на вашу схему базы данных их файла классов, если вы используете `declarative_base`.

При корректной настройке ``Alembic`` будет сравнивать текущее состояние базы данных и создавать файл различий между текущим состоянием базы данных и состоянием, указанным в вашем файле классов. Таким образом, для корректной работы этой функции вам не нужно вносить изменения в базу данных после изменения классов. Вам нужно запустить ``Alembic``, создать файл миграции и применить его. Тогда всё будет работать корректно. Если же вы примените изменения к базе данных в ручную ``Alembic`` может из не заметить, но это не точно :D.  

## Использование

Итак, давайте разберём правильный алгоритм использования после первоначальной настройки. Этот шаг лучше делать или до создания классов, или до указания переменной `target_metadata` в файле `env.py`.

Я рекомендую создать пустую отправную точку. Для этого используем создание в ручном режиме и получим файл с пустыми идентификаторами.

```bash
alembic revision -m 'init'
```

Пример сгенерированного файла:

```python
"""init

Revision ID: df1c96c3b2f3
Revises:
Create Date: 2019-05-22 13:26:57.676250

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'df1c96c3b2f3'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
```

Как видно он пустой, что нам и требуется. В следующем файле уже будет ссылка на этот файл, что нам и требуется. Но для корректной работы мы должны сначала мигрировать нашу базу данных, согласно первому файлу. Для этого запускаем миграцию:

```bash
alembic upgrade head
```

Вот вывод команды, подтверждающий, что миграция прошла успешна. По завершению в базе данных будет создана служебная таблица `alembic_version`, в которой отслеживается текущая версия базы данных. В нашем случае `df1c96c3b2f3`.

```bash
INFO  [alembic.runtime.migration] Context impl MySQLImpl.
INFO  [alembic.runtime.migration] Will assume non-transactional DDL.
INFO  [alembic.runtime.migration] Running upgrade  -> df1c96c3b2f3, init
```

Теперь сгенерируем автоматическую миграцию, согласно параметрам, заданных в наших скриптах. Для этого достаточно ввести простую команду:

```bash
alembic revision -m 'Doctors' --autogenerate
```

Так как у меня в проекте уже много таблиц, то вывод будет следующим:

```bash
INFO  [alembic.runtime.migration] Context impl MySQLImpl.
INFO  [alembic.runtime.migration] Will assume non-transactional DDL.
INFO  [alembic.autogenerate.compare] Detected added table 'doctors_profiles'
INFO  [alembic.autogenerate.compare] Detected added table 'reviews'
INFO  [alembic.autogenerate.compare] Detected added table 'service'
INFO  [alembic.autogenerate.compare] Detected added table 'teleconsult'
INFO  [alembic.autogenerate.compare] Detected added table 'webconsult'
  Generating /home/insspb/doctors/db/versions/3aac650aba32_doctors.py ... done
```

Если мы откроем получившийся файл, то в нём мы увидим код, который ссылается на нашу отправную точку и позволяет как обновлять базу данных, так и откатывать это обновление. Таким образом в разработке можно генерировать несколько версий подобного файла, стирая неудачные варианты и не проводя всю цепочку, в то время как в продакшене сохранится вся нужная история.

Вот такой файл, сгенерировала автогенерация, его при необходимости можно исправить перед применением:

```python
"""Doctors

Revision ID: 3aac650aba32
Revises: df1c96c3b2f3
Create Date: 2019-05-22 13:36:44.737592

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '3aac650aba32'
down_revision = 'df1c96c3b2f3'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('doctors_profiles',
    sa.Column('id', sa.BigInteger(), autoincrement=True, nullable=False),
    sa.Column('profile_url', sa.Text(length=4294000000), nullable=True),
    sa.Column('source_discipline', sa.String(length=255), nullable=True),
    sa.Column('source_department', sa.String(length=255), nullable=True),
    sa.Column('source_page_url', sa.Text(length=4294000000), nullable=True),
    sa.Column('parsed', sa.Boolean(), nullable=True),
    sa.Column('parsed_date', sa.DateTime(), nullable=True),
    sa.PrimaryKeyConstraint('id'),
    sa.UniqueConstraint('id')
    )
    op.create_table('reviews',
    sa.Column('id', sa.BigInteger(), autoincrement=True, nullable=False),
    sa.PrimaryKeyConstraint('id'),
    sa.UniqueConstraint('id')
    )
    op.create_table('service',
    sa.Column('id', sa.BigInteger(), autoincrement=True, nullable=False),
    sa.Column('doc', sa.String(length=255), nullable=True),
    sa.Column('doc_hosp', sa.String(length=255), nullable=True),
    sa.Column('doc_dept', sa.String(length=255), nullable=True),
    sa.Column('disease', sa.String(length=255), nullable=True),
    sa.Column('patient', sa.String(length=255), nullable=True),
    sa.Column('geo_loc', sa.String(length=255), nullable=True),
    sa.Column('source', sa.String(length=255), nullable=True),
    sa.Column('time', sa.DateTime(), nullable=True),
    sa.Column('content', sa.Text(length=4294000000), nullable=True),
    sa.Column('doc_reply', sa.Boolean(), nullable=True),
    sa.Column('url', sa.Text(length=4294000000), nullable=True),
    sa.PrimaryKeyConstraint('id'),
    sa.UniqueConstraint('id')
    )
    op.create_table('teleconsult',
    sa.Column('id', sa.BigInteger(), autoincrement=True, nullable=False),
    sa.Column('doc', sa.String(length=255), nullable=True),
    sa.Column('doc_hosp', sa.String(length=255), nullable=True),
    sa.Column('doc_dept', sa.String(length=255), nullable=True),
    sa.Column('patient', sa.String(length=255), nullable=True),
    sa.Column('use', sa.String(length=255), nullable=True),
    sa.Column('time', sa.DateTime(), nullable=True),
    sa.Column('content', sa.Text(length=4294000000), nullable=True),
    sa.Column('tag1', sa.String(length=255), nullable=True),
    sa.Column('tag2', sa.String(length=255), nullable=True),
    sa.Column('tag3', sa.String(length=255), nullable=True),
    sa.Column('tag4', sa.String(length=255), nullable=True),
    sa.Column('tag5', sa.String(length=255), nullable=True),
    sa.Column('tag6', sa.String(length=255), nullable=True),
    sa.Column('tag7', sa.String(length=255), nullable=True),
    sa.Column('tag8', sa.String(length=255), nullable=True),
    sa.Column('tag9', sa.String(length=255), nullable=True),
    sa.Column('tag10', sa.String(length=255), nullable=True),
    sa.PrimaryKeyConstraint('id'),
    sa.UniqueConstraint('id')
    )
    op.create_table('webconsult',
    sa.Column('id', sa.BigInteger(), autoincrement=True, nullable=False),
    sa.Column('doc', sa.String(length=255), nullable=True),
    sa.Column('states', sa.String(length=255), nullable=True),
    sa.Column('patient', sa.String(length=255), nullable=True),
    sa.Column('doc_hospital', sa.String(length=255), nullable=True),
    sa.Column('doc_dept', sa.String(length=255), nullable=True),
    sa.Column('title', sa.String(length=255), nullable=True),
    sa.Column('disease', sa.String(length=255), nullable=True),
    sa.Column('duration', sa.String(length=255), nullable=True),
    sa.Column('pragnent', sa.String(length=255), nullable=True),
    sa.Column('allergy', sa.String(length=255), nullable=True),
    sa.Column('description', sa.String(length=255), nullable=True),
    sa.Column('past_medicine_record', sa.String(length=255), nullable=True),
    sa.Column('target', sa.String(length=255), nullable=True),
    sa.Column('hospital', sa.String(length=255), nullable=True),
    sa.Column('medicine_history', sa.String(length=255), nullable=True),
    sa.Column('lab_results', sa.String(length=255), nullable=True),
    sa.Column('posted_by', sa.String(length=255), nullable=True),
    sa.Column('date', sa.String(length=255), nullable=True),
    sa.Column('content', sa.Text(length=4294000000), nullable=True),
    sa.Column('voice_content', sa.String(length=255), nullable=True),
    sa.Column('voice_length', sa.String(length=255), nullable=True),
    sa.PrimaryKeyConstraint('id'),
    sa.UniqueConstraint('id')
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('webconsult')
    op.drop_table('teleconsult')
    op.drop_table('service')
    op.drop_table('reviews')
    op.drop_table('doctors_profiles')
    # ### end Alembic commands ###
```

Осталось применить изменения, тут всё как и раньше. Команда:

```bash
alembic upgrade head
```

Вывод:

```bash
INFO  [alembic.runtime.migration] Context impl MySQLImpl.
INFO  [alembic.runtime.migration] Will assume non-transactional DDL.
INFO  [alembic.runtime.migration] Running upgrade df1c96c3b2f3 -> 3aac650aba32, Doctors
```

На этом вводная часть закончена, теперь я рекомендую ознакомиться с официальной документацией, а конкретнее с разделом, что отслеживает и что не отслеживает автоматическое генерирование миграций.

Ссылка: https://alembic.sqlalchemy.org/en/latest/autogenerate.html#what-does-autogenerate-detect-and-what-does-it-not-detect
